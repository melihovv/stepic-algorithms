#include <gtest/gtest.h>
#include <vector>
#include <tuple>
#include <cstdint>
#include "../fibonacci/fibonacci.h"

class TestFibonacci : public ::testing::Test
{
protected:
    static const std::vector<std::pair<int, int>> fibonacciInput;
    static const std::vector<std::pair<int, int>> lastDigitFibonacciInput;
    static const std::vector<std::tuple<std::uint64_t, int, int>> fibNModMInput;
};

const std::vector<std::pair<int, int>> TestFibonacci::fibonacciInput = {
    {0, 0},
    {1, 1},
    {2, 1},
    {3, 2},
    {4, 3},
    {5, 5},
    {6, 8},
    {7, 13},
    {8, 21},
    {9, 34},
    {10, 55},
    {11, 89},
    {12, 144},
    {13, 233},
    {14, 377},
    {15, 610},
    {16, 987},
    {17, 1597},
    {18, 2584},
    {19, 4181},
    {20, 6765},
    {21, 10946},
    {22, 17711},
    {23, 28657},
    {24, 46368},
    {25, 75025},
    {26, 121393},
    {27, 196418},
    {28, 317811},
    {29, 514229},
    {30, 832040},
    {31, 1346269},
    {32, 2178309},
    {33, 3524578},
    {34, 5702887},
    {35, 9227465},
    {36, 14930352},
    {37, 24157817},
    {38, 39088169},
    {39, 63245986},
    {40, 102334155}
};

TEST_F(TestFibonacci, Fibonacci)
{
    for (auto pair : fibonacciInput)
    {
        ASSERT_EQ(pair.second, fibonacci(pair.first));
    }
}

const std::vector<std::pair<int, int>> TestFibonacci::lastDigitFibonacciInput =
{
    {0, 0},
    {1, 1},
    {2, 1},
    {3, 2},
    {4, 3},
    {5, 5},
    {6, 8},
    {7, 3},
    {8, 1},
    {38, 9},
    {39, 6},
    {40, 5},
    {60, 0},
    {725516, 7}
};

TEST_F(TestFibonacci, LastDigitOfFibonacci)
{
    for (auto pair : lastDigitFibonacciInput)
    {
        ASSERT_EQ(pair.second, lastDigitOfFibonacci(pair.first));
    }
}

const std::vector<std::tuple<std::uint64_t, int, int>>
TestFibonacci::fibNModMInput = std::vector<std::tuple<std::uint64_t, int, int>>
{
    std::make_tuple(1, 2, 1),
    std::make_tuple(1, 3, 1),
    std::make_tuple(1, 100, 1),
    std::make_tuple(2, 2, 1),
    std::make_tuple(2, 3, 1),
    std::make_tuple(2, 4, 1),
    std::make_tuple(2, 5, 1),
    std::make_tuple(2, 6, 1),
    std::make_tuple(2, 7, 1),
    std::make_tuple(2, 100, 1),
    std::make_tuple(3, 2, 0),
    std::make_tuple(3, 3, 2),
    std::make_tuple(3, 100, 2),
    std::make_tuple(3, 10, 2),
    std::make_tuple(18, 6, 4),
    std::make_tuple(18, 7, 1),
    std::make_tuple(18, 8, 0),
    std::make_tuple(18, 9, 1),
    std::make_tuple(39, 7, 6),
    std::make_tuple(47, 3, 1),
    std::make_tuple(56, 4, 1),
    std::make_tuple(56, 9, 3),
    std::make_tuple(56, 11, 8),
    std::make_tuple(16759108071127739, 44408, 22937),
};

TEST_F(TestFibonacci, FibonacciModM)
{
    for (auto t : fibNModMInput)
    {
        ASSERT_EQ(std::get<2>(t), fibonacciModM(std::get<0>(t), std::get<1>(t)));
    }
}
