#include <gtest/gtest.h>
#include "../gcd/gcd.h"

class TestGcd : public ::testing::Test
{
protected:
    static const std::vector<std::tuple<int, int, int>> gcdInput;
};

const std::vector<std::tuple<int, int, int>>
TestGcd::gcdInput = std::vector<std::tuple<int, int, int>> {
    std::make_tuple(1, 1, 1),
    std::make_tuple(1, 2000000000, 1),
    std::make_tuple(2000000000, 2000000000, 2000000000),
    std::make_tuple(18, 35, 1),
    std::make_tuple(14159572, 63967072, 4),
};

TEST_F(TestGcd, Gcd)
{
    for (auto t : gcdInput)
    {
        ASSERT_EQ(std::get<2>(t), gcd(std::get<0>(t), std::get<1>(t)));
    }
}
