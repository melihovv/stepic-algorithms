#ifndef FIBONACCI_H
#define FIBONACCI_H

long long int fibonacci(int n);
int lastDigitOfFibonacci(int n);
int fibonacciModM(unsigned long long n, int m);

#endif // FIBONACCI_H
