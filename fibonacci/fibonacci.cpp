#include "fibonacci.h"
#include <cmath>
#include <vector>
#include <cstdint>

long long int fibonacci(int n)
{
    long long int a = 0;
    long long int b = 1;

    for (int i = 0; i < n; ++i)
    {
        long long int temp = a + b;
        a = b;
        b = temp;
    }

    return a;
}

int lastDigitOfFibonacci(int n)
{
    int a = 0;
    int b = 1;

    for (int i = 0; i < n; ++i)
    {
        int temp = (a + b) % 10;
        a = b;
        b = temp;
    }

    return a;
}

int fibonacciModM(std::uint64_t n, int m)
{
    // t <= 6m
    // 1 <= n <= 10^18
    // 2 <= m <= 10^5
    int t = 0;
    std::vector<int> v = {0, 1};
    v.reserve(6 * m);

    for (int i = 2; i < 6 * m; ++i)
    {
        v.push_back((v[i - 1] + v[i - 2]) % m);
        ++t;

        if (v[i] == 1 && v[i - 1] == 0)
        {
            break;
        }
    }

    if (n == 1 || n == 2)
    {
        return 1;
    }

    return v[n % t];
}
