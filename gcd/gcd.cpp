#include "gcd.h"
#include <algorithm>

int gcd(int a, int b)
{
    // 1 <= a, b <= 2 * 10^9
    while (b)
    {
        a %= b;
        std::swap(a, b);
    }

    return a;
}
